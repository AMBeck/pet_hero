﻿// Base class for all Items

public class Item
{
    private string name = "Generic Item";

	public Item(string name)
    {
        this.name = name;
    }

    public string GetName()
    {
        return name;
    }
}
