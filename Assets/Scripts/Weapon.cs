﻿// Parent of all weapon classes. Derived from Item class.

public class Weapon : Item {

    private float damage = 1.0f;
    private float attackSpeed = 1.0f;

	public Weapon(string name, float damage, float attackSpeed) : base(name)
    {
        this.damage = damage;
        this.attackSpeed = attackSpeed;
    }

    public float GetDamage()
    {
        return damage;
    }

    public float GetAttackSpeed()
    {
        return attackSpeed;
    }
}
